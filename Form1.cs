﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DailyCount
{
    public partial class MainWin : Form
    {
        string d1, d2;
        public MainWin()
        {
            InitializeComponent();
        }

        private void ClockLabel_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            timer_clock.Start();
            d1 = (new DateTime(2022, 2, 13) - DateTime.Now).TotalDays.ToString("F1");
            d2 = (new DateTime(2024, 6, 7) - DateTime.Now).TotalDays.ToString("F1");
            listView1.Items.Add("开学 - 2022年2月13日 - 仅" + d1 + "天");
            listView1.Items.Add("高考 - 2024年6月7日 - 仅" + d2 + "天");
            
            ClockLabel.Text = DateTime.Now.ToString();
            line.Text = new string('-', ClockLabel.Text.Length * 4);

        }

        private void timer_clock_Tick(object sender, EventArgs e)
        {
            ClockLabel.Text = DateTime.Now.ToString();

            if (d1 != (new DateTime(2022, 2, 13) - DateTime.Now).TotalDays.ToString("F1"))
            {
                listView1.Items.Clear();
                listView1.Items.Add("开学 - 2022年2月13日 - 仅" + d1 + "天");
                listView1.Items.Add("高考 - 2024年6月7日 - 仅" + d2 + "天");
            }
            int hour = DateTime.Now.Hour; string str;
            if (hour >= 20)
                str = "晚上好~";
            else if (hour < 5)
                str = "夜深了，熬夜对身体不好哦！";
            else if (hour < 8)
                str = "早上好，开启活力满满的一天吧！";
            else if (hour < 12)
                str = "上午好~";
            else if (hour < 14)
                str = "中午好！子曰：「中午不睡，下午崩溃。」要记得睡午觉哦~";
            else if (hour < 18)
                str = "下午好~";
            else
                str = "傍晚了，该吃晚饭了~";
            var d3 = (new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day + 1) - DateTime.Now).TotalHours.ToString("F1");
            tips.Text = str;
            float f = (hour / 24.0f) * 100;
            last.Text = "今天已过" + f.ToString("F2") + "%， " + "距离今天结束还有" + d3 + "小时。";

        }

        private void ishd_Click(object sender, EventArgs e)
        {
            //is hide
            if (this.FormBorderStyle == FormBorderStyle.None)
            {
                ishd.Text = "固定组件";
                this.FormBorderStyle = FormBorderStyle.Sizable;
            }
            else
            {
                ishd.Text = "取消固定";
                this.FormBorderStyle = FormBorderStyle.None;
            }
        }

        private void exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void sync_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            listView1.Items.Add("开学 - 2022年2月13日 - 仅" + d1 + "天");
            listView1.Items.Add("高考 - 2024年6月7日 - 仅" + d2 + "天");

            int hour = DateTime.Now.Hour;
            string str;
            if (hour >= 20)
                str = "晚上好~";
            else if (hour < 5)
                str = "夜深了，熬夜对身体不好哦！";
            else if (hour < 8)
                str = "早上好，开启活力满满的一天吧！";
            else if (hour < 12)
                str = "上午好~";
            else if (hour < 14)
                str = "中午好！子曰：「中午不睡，下午崩溃。」要记得睡午觉哦~";
            else if (hour < 18)
                str = "下午好~";
            else
                str = "傍晚了，该吃晚饭了~";
            var d3 = (new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day + 1) - DateTime.Now).TotalHours.ToString("F1");
            tips.Text = str;
            float f = (hour / 24.0f) * 100;
            last.Text = "今天已过" + f.ToString("F2") + "%， " + "距离今天结束还有" + d3 + "小时。";
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
