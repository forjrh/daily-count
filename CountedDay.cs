﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Globalization;


namespace DailyCount
{
    internal class CountedDay
    {
        public CountedDay(DateTime dt, string name)
        {
            this.name = name;
            this.date = dt;
        }

        public DateTime date { set; get; }
        public string name { set; get; }

        public TimeSpan toNow()
        {
            return this.date - DateTime.Now;
        }
    }
}
