﻿namespace DailyCount
{
    partial class MainWin
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWin));
            this.ClockLabel = new System.Windows.Forms.Label();
            this.timer_clock = new System.Windows.Forms.Timer(this.components);
            this.ishd = new System.Windows.Forms.Label();
            this.line = new System.Windows.Forms.Label();
            this.setting = new System.Windows.Forms.Label();
            this.DayCount = new System.Windows.Forms.GroupBox();
            this.listView1 = new System.Windows.Forms.ListView();
            this.exit = new System.Windows.Forms.Label();
            this.sync = new System.Windows.Forms.Label();
            this.tips = new System.Windows.Forms.Label();
            this.last = new System.Windows.Forms.Label();
            this.DayCount.SuspendLayout();
            this.SuspendLayout();
            // 
            // ClockLabel
            // 
            this.ClockLabel.AutoSize = true;
            this.ClockLabel.BackColor = System.Drawing.Color.Aqua;
            this.ClockLabel.Font = new System.Drawing.Font("微软雅黑", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ClockLabel.ForeColor = System.Drawing.Color.White;
            this.ClockLabel.Location = new System.Drawing.Point(6, 9);
            this.ClockLabel.Name = "ClockLabel";
            this.ClockLabel.Size = new System.Drawing.Size(110, 46);
            this.ClockLabel.TabIndex = 0;
            this.ClockLabel.Text = "clock";
            this.ClockLabel.Click += new System.EventHandler(this.ClockLabel_Click);
            // 
            // timer_clock
            // 
            this.timer_clock.Tick += new System.EventHandler(this.timer_clock_Tick);
            // 
            // ishd
            // 
            this.ishd.AutoSize = true;
            this.ishd.BackColor = System.Drawing.Color.Transparent;
            this.ishd.ForeColor = System.Drawing.Color.White;
            this.ishd.Location = new System.Drawing.Point(12, 71);
            this.ishd.Name = "ishd";
            this.ishd.Size = new System.Drawing.Size(53, 12);
            this.ishd.TabIndex = 1;
            this.ishd.Text = "固定组件";
            this.ishd.Click += new System.EventHandler(this.ishd_Click);
            // 
            // line
            // 
            this.line.AutoSize = true;
            this.line.ForeColor = System.Drawing.Color.White;
            this.line.Location = new System.Drawing.Point(12, 55);
            this.line.Name = "line";
            this.line.Size = new System.Drawing.Size(131, 12);
            this.line.TabIndex = 2;
            this.line.Text = "---------------------\r\n";
            // 
            // setting
            // 
            this.setting.AutoSize = true;
            this.setting.BackColor = System.Drawing.Color.Transparent;
            this.setting.ForeColor = System.Drawing.Color.White;
            this.setting.Location = new System.Drawing.Point(71, 71);
            this.setting.Name = "setting";
            this.setting.Size = new System.Drawing.Size(29, 12);
            this.setting.TabIndex = 3;
            this.setting.Text = "设置";
            // 
            // DayCount
            // 
            this.DayCount.BackColor = System.Drawing.Color.Transparent;
            this.DayCount.Controls.Add(this.listView1);
            this.DayCount.ForeColor = System.Drawing.Color.White;
            this.DayCount.Location = new System.Drawing.Point(12, 153);
            this.DayCount.Name = "DayCount";
            this.DayCount.Size = new System.Drawing.Size(386, 256);
            this.DayCount.TabIndex = 4;
            this.DayCount.TabStop = false;
            this.DayCount.Text = "倒计日";
            // 
            // listView1
            // 
            this.listView1.BackColor = System.Drawing.Color.Aqua;
            this.listView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listView1.Font = new System.Drawing.Font("宋体", 13F);
            this.listView1.ForeColor = System.Drawing.Color.White;
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(6, 13);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(374, 237);
            this.listView1.TabIndex = 0;
            this.listView1.TabStop = false;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.List;
            this.listView1.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            // 
            // exit
            // 
            this.exit.AutoSize = true;
            this.exit.BackColor = System.Drawing.Color.Transparent;
            this.exit.ForeColor = System.Drawing.Color.White;
            this.exit.Location = new System.Drawing.Point(141, 71);
            this.exit.Name = "exit";
            this.exit.Size = new System.Drawing.Size(29, 12);
            this.exit.TabIndex = 5;
            this.exit.Text = "退出";
            this.exit.Click += new System.EventHandler(this.exit_Click);
            // 
            // sync
            // 
            this.sync.AutoSize = true;
            this.sync.BackColor = System.Drawing.Color.Transparent;
            this.sync.ForeColor = System.Drawing.Color.White;
            this.sync.Location = new System.Drawing.Point(106, 71);
            this.sync.Name = "sync";
            this.sync.Size = new System.Drawing.Size(29, 12);
            this.sync.TabIndex = 6;
            this.sync.Text = "刷新";
            this.sync.Click += new System.EventHandler(this.sync_Click);
            // 
            // tips
            // 
            this.tips.AutoSize = true;
            this.tips.BackColor = System.Drawing.Color.Transparent;
            this.tips.Font = new System.Drawing.Font("宋体", 12F);
            this.tips.ForeColor = System.Drawing.Color.White;
            this.tips.Location = new System.Drawing.Point(12, 96);
            this.tips.Name = "tips";
            this.tips.Size = new System.Drawing.Size(39, 16);
            this.tips.TabIndex = 7;
            this.tips.Text = "提示";
            // 
            // last
            // 
            this.last.AutoSize = true;
            this.last.BackColor = System.Drawing.Color.Transparent;
            this.last.Font = new System.Drawing.Font("宋体", 12F);
            this.last.ForeColor = System.Drawing.Color.White;
            this.last.Location = new System.Drawing.Point(12, 125);
            this.last.Name = "last";
            this.last.Size = new System.Drawing.Size(47, 16);
            this.last.TabIndex = 8;
            this.last.Text = "提示2";
            // 
            // MainWin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Aqua;
            this.ClientSize = new System.Drawing.Size(402, 411);
            this.Controls.Add(this.last);
            this.Controls.Add(this.tips);
            this.Controls.Add(this.sync);
            this.Controls.Add(this.exit);
            this.Controls.Add(this.DayCount);
            this.Controls.Add(this.setting);
            this.Controls.Add(this.line);
            this.Controls.Add(this.ishd);
            this.Controls.Add(this.ClockLabel);
            this.ForeColor = System.Drawing.Color.White;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainWin";
            this.ShowInTaskbar = false;
            this.Text = "Daily Count : 倒计日";
            this.TransparencyKey = System.Drawing.Color.Aqua;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.DayCount.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label ClockLabel;
        private System.Windows.Forms.Timer timer_clock;
        private System.Windows.Forms.Label ishd;
        private System.Windows.Forms.Label line;
        private System.Windows.Forms.Label setting;
        private System.Windows.Forms.GroupBox DayCount;
        private System.Windows.Forms.Label exit;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.Label sync;
        private System.Windows.Forms.Label tips;
        private System.Windows.Forms.Label last;
    }
}

